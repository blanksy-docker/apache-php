# xalauc/php

## Tags

| Tag | Folder | Description |
| - | - | - |
| `7.1`, `latest` | `7.1` | PHP 7.1 |
| `7.0` | `7.0` | PHP 7.0 |
| `5.6` | `5.6` | PHP 5.6 |

## Setting up a project

To start the container you need to run a command like the following:

    docker run \
        --name [CONTAINER_NAME] \
        -p 80:80 \
        -p 443:443 \
        -v $(pwd)/www:/var/www \
        -v $(pwd)/sites-enabled:/etc/apache2/sites-enabled \
        -v $(pwd)/docker-tools:/docker-tools \
        -d xalauc/apache-php:latest

* The volume `/var/www` is the location of the web files.
* The volume `/etc/apache2/sites-enabled` is the location of any virtual hosts for the application.
* The volume `/docker-tools` is the location of any custom startup scripts when the docker machine is started.

## Volumes

### /var/www

Pretty self explanatory really.

### /etc/apache/sites-enabled

Place any virtual host files into this volume with an extension of `.conf` and
they will automatically be loaded into apache.

### /docker-tools

Any scripts placed in this volume will automatically run when the machine is started.

Using this method you can do things such as setting of the timezone:

    ln -sf /usr/share/zoneinfo/Australia/Sydney /etc/localtime

## Running composer from the docker host

    docker exec --user [CONTAINER_NAME] bash -c 'cd /var/www/ && composer [OPTIONS_HERE]'

## Running phpUnit from the docker host

    docker exec --user [CONTAINER_NAME] bash -c 'cd /var/www/ && phpunit [OPTIONS_HERE]'

## Accessing the container (You should not really need to but if you need to)
Connecting to the virtual machine, good for checking logs and the likes,
also good for actioning commands for some applications that require command line access.

    docker exec -it [CONTAINER_NAME] bash

## License (MIT)
Copyright (c) 2016 Ben Blanks

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
