#!/bin/bash

if [ "$1" == "" ];
then
    echo ""
    echo "ERROR: You must enter a PHP version as the first parameter, try '5.6'"
    echo ""

    exit 1;
fi

if [ -d "./$1" ];
then
    echo "PHP version $1 found, building and starting the container now."
    echo ""
    echo ""

    cd "$1"
    docker build -t "xalauc/apache-php:test-$1" .

    docker run --name="apache-php-test-$1" \
        -p 9086:80 \
        -v $(pwd)/../test/docker-tools:/docker-tools \
        -v $(pwd)/../test/www:/var/www \
        -v $(pwd)/../test/sites-enabled:/etc/apache2/sites-enabled \
        -d "xalauc/apache-php:test-$1" > /dev/null

else
    echo "That PHP version does not exist, please type another PHP version to attemp it."
fi

pause
clear

echo ""
echo "-----------------------------------------------------------------------------------"
echo " _  _ ____ _    ____ _  _ ____   / ____ ___  ____ ____ _  _ ____    ___  _  _ ___  "
echo "  \/  |__| |    |__| |  | |     /  |__| |__] |__| |    |__| |___ __ |__] |__| |__] "
echo " _/\_ |  | |___ |  | |__| |___ /   |  | |    |  | |___ |  | |___    |    |  | |    "
echo "                                                                                   "
echo "-----------------------------------------------------------------------------------"
echo ""
echo "You can now access your machine in your browser of choice using:"
echo ""
echo "    http://localhost:9086/"
echo ""
echo "When you are finished you can stop the docker machine by running:"
echo ""
echo "    docker stop apache-php-test-$1 && docker rm apache-php-test-$1"
echo ""